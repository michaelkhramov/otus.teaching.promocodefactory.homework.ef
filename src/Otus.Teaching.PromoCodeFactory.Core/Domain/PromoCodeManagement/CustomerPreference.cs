﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Customer Preference
    /// </summary>
    public class CustomerPreference: BaseEntity
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Preference Id
        /// </summary>
        public Guid PreferenceId { get; set; }

        /// <summary>
        /// Preference
        /// </summary>
        public virtual Preference Preference { get; set; }
    }
}
