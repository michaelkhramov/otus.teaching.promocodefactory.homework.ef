﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Preference
    /// </summary>
    public class Preference: BaseEntity
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Customers
        /// </summary>
        public virtual ICollection<CustomerPreference> Customers { get; set; }
    }
}