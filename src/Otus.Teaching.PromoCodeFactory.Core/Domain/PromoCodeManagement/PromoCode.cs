﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Promo Code
    /// </summary>
    public class PromoCode: BaseEntity
    {
        /// <summary>
        /// Code
        /// </summary>        
        public string Code { get; set; }

        /// <summary>
        /// Service Info
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Begin Date
        /// </summary>
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// End Date
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Partner Name
        /// </summary>
        public string PartnerName { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Partner Manager
        /// </summary>
        public virtual Employee PartnerManager { get; set; }

        /// <summary>
        /// Preference
        /// </summary>
        public virtual Preference Preference { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public virtual Customer Customer { get; set; }
    }
}