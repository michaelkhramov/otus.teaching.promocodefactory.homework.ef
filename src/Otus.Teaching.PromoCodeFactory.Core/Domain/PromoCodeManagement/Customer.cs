﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Customer
    /// </summary>
    public class Customer: BaseEntity
    {
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Full Name
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Preferences
        /// </summary>
        public virtual ICollection<CustomerPreference> Preferences { get; set; }

        /// <summary>
        /// Promo Codes
        /// </summary>
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}