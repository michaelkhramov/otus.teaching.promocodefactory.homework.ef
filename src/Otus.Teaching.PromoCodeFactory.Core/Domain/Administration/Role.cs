﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Role
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// Name        
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Employees
        /// </summary>        
        public virtual ICollection<Employee> Employees { get; set; }
    }
}