﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Employee
    /// </summary>
    public class Employee : BaseEntity
    {
        /// <summary>
        /// First Name
        /// </summary>        
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// Full Name
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Applied Promocodes Count
        /// </summary>
        public int AppliedPromocodesCount { get; set; }

        /// <summary>
        /// Role Id
        /// </summary>        
        public Guid RoleId { get; set; }

        /// <summary>
        /// Role
        /// </summary>                
        public virtual Role Role { get; set; }
    }
}