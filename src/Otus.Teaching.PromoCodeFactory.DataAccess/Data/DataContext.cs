﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    /// <summary>
    /// Promo Code Factory Context
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Roles
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Employees
        /// </summary>
        public DbSet<Employee> Employees { get; set; }        

        /// <summary>
        /// Customer
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Preferences
        /// </summary>
        public DbSet<Preference> Preferences { get; set; }

        /// <summary>
        /// PromoCodes
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// On Mode lCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Employee
            modelBuilder.Entity<Employee>().HasKey(e => e.Id);
            modelBuilder.Entity<Employee>().Property(e => e.FirstName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(e => e.Email).IsRequired().HasMaxLength(100);

            //Role
            modelBuilder.Entity<Role>().HasKey(r => r.Id);
            modelBuilder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(254);

            //Customer
            modelBuilder.Entity<Customer>().HasKey(r => r.Id);
            modelBuilder.Entity<Customer>().Property(r => r.FirstName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(r => r.LastName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(r => r.Email).IsRequired().HasMaxLength(100);

            //Preference
            modelBuilder.Entity<Preference>().HasKey(p => p.Id);
            modelBuilder.Entity<Preference>().Property(p => p.Name).IsRequired().HasMaxLength(100);

            //Promocode
            modelBuilder.Entity<PromoCode>().HasKey(p => p.Id);
            modelBuilder.Entity<PromoCode>().Property(p => p.Code).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).IsRequired().HasMaxLength(100);

            //Relationship between Employee and Role
            modelBuilder.Entity<Employee>()
                        .HasOne(p => p.Role)
                        .WithMany(p => p.Employees)
                        .HasForeignKey(p => p.RoleId);

            //Relationships between Customer and Promocode (one-to-many)
            modelBuilder.Entity<Customer>()
                            .HasMany(r => r.PromoCodes)
                            .WithOne(e => e.Customer)
                            .HasForeignKey(e => e.CustomerId);

            //Relationships between customer and preferences (many-to-many)
            modelBuilder.Entity<CustomerPreference>().HasKey(r => r.Id);
            modelBuilder.Entity<CustomerPreference>()
                                .HasOne(p => p.Customer)
                                .WithMany(p => p.Preferences)
                                .HasForeignKey(p => p.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                                .HasOne(p => p.Preference)
                                .WithMany(p => p.Customers)
                                .HasForeignKey(p => p.PreferenceId);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);

            base.OnModelCreating(modelBuilder);
        }
    }
}
