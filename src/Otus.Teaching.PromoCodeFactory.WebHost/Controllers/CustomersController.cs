﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController: ControllerBase
    {
        /// <summary>
        /// Repository
        /// </summary>
        private readonly IRepository<Customer> _repositoryCustomers;
        private readonly IRepository<Preference> _repositoryPreferences;

        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="repositoryCustomers">Customer Repository</param>
        /// <param name="repositoryPreferences">Preference Repository</param>
        public CustomersController(IRepository<Customer> repositoryCustomers, IRepository<Preference> repositoryPreferences)
        {
            _repositoryCustomers = repositoryCustomers;
            _repositoryPreferences = repositoryPreferences;
        }

        /// <summary>
        /// Gets List of Customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerResponse>> GetCustomersAsync()
        {
            var customers = await _repositoryCustomers.GetAllAsync();
         
            var list = customers.Select(customer => CreateCustomerResponse(customer)).ToList();

            return Ok(list); 
        }
        
        /// <summary>
        /// Gets Customer By Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repositoryCustomers.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = CreateCustomerResponse(customer);

            return Ok(customerModel);
        }

        /// <summary>
        /// Creates Customer
        /// </summary>        
        /// <param name="request">Create Or Edit CustomerRequest</param>
        /// <returns>Customer Id</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var allPreferences = await _repositoryPreferences.GetAllAsync();
            var preferences = allPreferences.Where(p => request.PreferenceIds.Contains(p.Id));            
 
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = new List<CustomerPreference>()
            };

            foreach (var item in preferences)
            {
                customer.Preferences.Add(new CustomerPreference() { CustomerId = customer.Id, PreferenceId = item.Id });
            }

            await _repositoryCustomers.AddAsync(customer);
            
            return Ok(customer.Id);

        }

        /// <summary>
        /// Updates Customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Create Or Edit CustomerRequest</param> 
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _repositoryCustomers.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            
            customer.Preferences.Clear();            
            customer.Preferences = new List<CustomerPreference>();

            var allPreferences = await _repositoryPreferences.GetAllAsync();
            var preferences = allPreferences.Where(p => request.PreferenceIds.Contains(p.Id));            

            foreach (var item in preferences)
            {
                customer.Preferences.Add(new CustomerPreference() { CustomerId = customer.Id, PreferenceId = item.Id });
            }

            await _repositoryCustomers.UpdateAsync(customer);

            return Ok();
        }
        
        /// <summary>
        /// Deletes Customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpDelete]
        public async  Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _repositoryCustomers.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _repositoryCustomers.RemoveAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Creates Customer Response from Customer
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Customer Response</returns>
        private CustomerResponse CreateCustomerResponse(Customer customer)
        {
            return new CustomerResponse()
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Id = customer.Id,
                PromoCodes = customer.PromoCodes?.Select(promoCode => new PromoCodeShortResponse()
                {
                    Id = promoCode.Id,
                    BeginDate = promoCode.BeginDate.ToString(),
                    Code = promoCode.Code,
                    EndDate = promoCode.EndDate.ToString(),
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo
                }).ToList(),
                Preferences = customer.Preferences?.Select(preference => new PreferenceResponse()
                {
                    Id = preference.PreferenceId,
                    Name = preference.Preference?.Name
                }).ToList()
            };
        }
    }
}