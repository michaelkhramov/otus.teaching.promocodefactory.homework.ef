﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Preference
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        /// <summary>
        /// Repository
        /// </summary>
        private readonly IRepository<Preference> _repository;

        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="repository">Repositoryr</param>
        public PreferencesController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets List of Preferences
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await _repository.GetAllAsync();
         
            var list = preferences.Select(x =>
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return Ok(list); 
        }
    }
}