﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Customer Response
    /// </summary>
    public class CustomerResponse
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Fist Name
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Emails
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Preferences
        /// </summary>
        public List<PreferenceResponse> Preferences { get; set; }

        /// <summary>
        /// Promo Codes
        /// </summary>
        public List<PromoCodeShortResponse> PromoCodes { get; set; }
    }
}