﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Promo Code Short Response
    /// </summary>
    public class PromoCodeShortResponse
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// ServiceInfo
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Begin Date
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// End Date
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Partner Name
        /// </summary>
        public string PartnerName { get; set; }
    }
}